set terminal pngcairo enhanced
set output "plot.png"
unset key
unset colorbox
set title "Energies in diagonal basis"
set xlabel "Time t in fs"
set ylabel "Energy in eV"
set cbrange [0:17]
set palette defined (0.0 "gray90", 1e-5 "gray60", 1e-4 "gray30", 1e-3 "orange", 1e-2 "red", 1e-1 "magenta", 1e-0 "blue", 10 "blue", 11 "green", 12 "red", 13 "turquoise", 14 "orange", 15 "cyan", 16 "brown", 17 "skyblue")

plot "output_data/expec.out" u 1:($4)               title "Total Energy" lw   0.50 lc rgbcolor "#000000" w l, \
""               u 1:($5):(abs($7)+10) title "State 1"     lw   4.50 pal w l, \
""               u 1:($5):(abs($9))    title "State 1"     lw   3.50 pal w l, \
""               u 1:($6):(abs($8)+10) title "State 2"     lw   4.50 pal w l, \
""               u 1:($6):(abs($10))    title "State 2"     lw   3.50 pal w l, \
""               u 1:($3)               title "Trajectory"   lw   1.00 lc rgbcolor "#000000" pt 6 w p


