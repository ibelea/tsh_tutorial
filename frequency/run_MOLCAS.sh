#!/bin/bash
#$ -N MCASfreque
#$ -S /bin/bash
#$ -cwd

PRIMARY_DIR=/userTMP/leamibele/ethylene/frequency
SCRATCH_DIR=/scratch/leamibele/ethylene

export MOLCAS=/home/leamibele/Softwares/OpenMolcas/build
export MOLCASMEM=500
export MOLCASDISK=0
export MOLCASRAMD=0
export MOLCAS_MOLDEN=ON

#export MOLCAS_CPUS=1
#export OMP_NUM_THREADS=1

export Project="MOLCAS"
export HomeDir=$PRIMARY_DIR
export CurrDir=$PRIMARY_DIR
export WorkDir=$SCRATCH_DIR/$Project/
ln -sf $WorkDir $CurrDir/WORK

cd $HomeDir
mkdir -p $WorkDir

cp $HomeDir/SingletOrbitals.RasOrb $WorkDir

$MOLCAS/bin/pymolcas MOLCAS.input &> $CurrDir/MOLCAS.log

cp $WorkDir/SingletOrbitals.* $HomeDir
#mkdir -p $HomeDir/TRD/
#cp $WorkDir/TRD2_* $HomeDir/TRD/
